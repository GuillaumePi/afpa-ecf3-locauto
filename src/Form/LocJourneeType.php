<?php

namespace App\Form;

use App\Entity\Louer;
use App\Entity\Vehicule;
use App\Repository\LouerRepository;
use App\Repository\VehiculeRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocJourneeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeLoc', HiddenType::class, [
                'data' => 1
            ])
            ->add('dateDebutLocPrev', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'required' => true,
                    'format' => 'dd/MM/yyyy HH:mm',
                    'label' => 'Date et heure de départ :',
                    'attr' => [
                        'class' => 'form-control datetimepicker-input',
                        'id' => 'datetimepicker1 datedebut',
                        'data-toggle' => 'datetimepicker',
                        'data-target' => '#datetimepicker1',
                        'placeholder' => 'jj/mm/aaaa hh:mm',],
                ])
            ->add('dateFinLocPrev', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'required' => true,
                    'format' => 'dd/MM/yyyy HH:mm',
                    'label' => 'Date et heure de retour :',
                    'attr' => [
                        'class' => 'form-control datetimepicker-input',
                        'id' => 'datetimepicker2 dateFin',
                        'data-toggle' => 'datetimepicker',
                        'data-target' => '#datetimepicker2',
                        'placeholder' => 'jj/mm/aaaa hh:mm',],
                ])
            ->add('assurance', CheckboxType::class, [
                'required' => false
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $louer = $event->getData();
                if (!$louer) {
                    return;
                }
                $form = $event->getForm();

                if ($louer->getDateDebutLocPrev() && $louer->getDateFinLocPrev()) {
                    $form->add('vehiculeId', EntityType::class, [
                        'class' => Vehicule::class,
                        'query_builder' => function (VehiculeRepository $vehiculeRepository) use ($louer) {
                            return $vehiculeRepository->createQueryBuilder('v')
                                ->join('v.locations', 'l', Expr\Join::WITH, 'v.id = l.vehiculeId')
                                ->where('(:dateDebut NOT BETWEEN l.dateDebutLocPrev AND l.dateFinLocPrev) AND (:dateFin NOT BETWEEN l.dateDebutLocPrev AND l.dateFinLocPrev)')
                                ->orWhere('v.id NOT IN (SELECT (lo.vehiculeId) FROM App\Entity\Louer lo)')
                                ->setParameter('dateDebut', $louer->getDateDebutLocPrev())
                                ->setParameter('dateFin', $louer->getDateFinLocPrev());
                        },
                        'choice_attr' => [
                            'class' => 'm-1'
                        ],
                        'expanded' => true,

                    ]);
                }else{
                    $form->add('vehiculeId', EntityType::class, [
                        'class' => Vehicule::class,
                        'choice_attr' => [
                            'class' => 'm-1'
                        ],
                        'expanded' => true,

                    ]);
                }

            })
            ;


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Louer::class,
        ]);
    }
}
