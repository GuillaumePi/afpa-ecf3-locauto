<?php


namespace App\Form;



use App\Entity\Categorie;
use App\Entity\Tarif;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TarifFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categorieId', EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => 'libelle',
            ])
            ->add('caution', MoneyType::class)
            ->add('assurance', MoneyType::class)
            ->add('journee', MoneyType::class)
            ->add('weekend', MoneyType::class)
            ->add('kilometre', MoneyType::class)
            ->add('remise', PercentType::class)
            ->add('dateDebut', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
            ->add('dateFin', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tarif::class,
        ]);
    }
}