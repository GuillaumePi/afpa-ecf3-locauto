<?php


namespace App\Form;


use App\Entity\Louer;
use App\Entity\Vehicule;
use App\Repository\VehiculeRepository;
use DateTime;
use Doctrine\ORM\Query\Expr;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocWEType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeLoc', HiddenType::class, [
                'data' => 2
            ])
            ->add('dateDebutLocPrev', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'required' => true,
                    'format' => 'dd/MM/yyyy',
                    'label' => 'Date et heure de départ :',
                    'attr' => [
                        'class' => 'form-control datetimepicker-input',
                        'id' => 'datetimepicker3',
                        'data-toggle'=> 'datetimepicker',
                        'data-target' => '#datetimepicker3',
                        'placeholder' => 'jj/mm/aaaa',],

                ])
            ->add('dateFinLocPrev', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'required' => true,
                    'format' => 'dd/MM/yyyy',
                    'label' => 'Date et heure de retour :',
                    'attr' => [
                        'class' => 'form-control datetimepicker-input',
                        'id' => 'datetimepicker4',
                        'data-toggle'=> 'datetimepicker',
                        'data-target' => '#datetimepicker4',
                        'placeholder' => 'jj/mm/aaaa',],

                ])
            ->add('assurance', CheckboxType::class, [
                'required' => false
            ])
            ->add('vehiculeId', EntityType::class, [
                'class' => Vehicule::class,
                'query_builder' => function (VehiculeRepository $vehiculeRepository) {
                    return $vehiculeRepository->createQueryBuilder('v')
                        ->join('v.locations', 'l', Expr\Join::WITH, 'v.id = l.vehiculeId')
                        ->where('(:dateDebut NOT BETWEEN l.dateDebutLocPrev AND l.dateFinLocPrev) AND (:dateFin NOT BETWEEN l.dateDebutLocPrev AND l.dateFinLocPrev)')
                        ->orWhere('v.id NOT IN (SELECT (lo.vehiculeId) FROM App\Entity\Louer lo)')
                        ->setParameter('dateDebut', new DateTime('now'))
                        ->setParameter('dateFin', new DateTime('15-06-2021 12:00:00'));
                },
                'choice_attr' => [
                    'class' => 'row m-1'
                ],
                'expanded' => true,
                'label' => 'Choisissez votre véhicule :'

            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Louer::class,
        ]);
    }
}