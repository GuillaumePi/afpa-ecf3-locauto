<?php


namespace App\Form;


use App\Entity\Louer;
use App\Entity\User;
use App\Entity\Vehicule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userId', EntityType::class, [
                'class' => User::class,
                'choice_label' => function (User $user){
                    return sprintf('%s %s -> %s', $user->getNom(), $user->getPrenom(), $user->getEmail());
                },
            ])
            ->add('vehiculeId', EntityType::class, [
                'class' => Vehicule::class,
                'choice_label' => function (Vehicule $vehicule){
                    return sprintf('%s %s -> %s', $vehicule->getMarque(), $vehicule->getModele(), $vehicule->getImmatriculation());
                },
            ])
            ->add('dateDebutLocPrev', DateTimeType::class,[
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy HH:mm',
            ])
            ->add('dateFinLocPrev', DateTimeType::class,[
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy HH:mm',
            ])
            ->add('typeLoc', ChoiceType::class, [
                'choices' => [
                    'A la journée' => 1,
                    'Week-end' => 2,
                ],
            ])
            ->add('assurance', CheckboxType::class, [
                'required' => false
            ])
            ->add('kilometrageInit', IntegerType::class, [
                'required' => false
            ])
            ->add('dateDebutLocReel', DateTimeType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy HH:mm',
                'required' => false
            ])
            ->add('dateFinLocReel', DateTimeType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy HH:mm',
                'required' => false
            ])
            ->add('kilometrageFinal', IntegerType::class, [
                'required' => false
            ])
            ->add('volumeCarburantFinal', IntegerType::class, [
                'required' => false
            ])
            ->add('montantRetenuCaution', MoneyType::class, [
                'required' => false
            ])
            ->add('raisonRetenuCaution', TextType::class, [
                'required' => false
            ])
            ->add('remarque', TextareaType::class, [
                'required' => false
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Louer::class,
        ]);
    }
}