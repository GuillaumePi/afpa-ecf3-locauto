<?php


namespace App\Form;


use App\Entity\Agence;
use App\Entity\Carburant;
use App\Entity\Categorie;
use App\Entity\Vehicule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehiculeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('carburantId', EntityType::class, [
               'class' => Carburant::class,
               'choice_label' => 'libelle'
            ])
            ->add('categorieId', EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => 'libelle'
            ])
            ->add('agenceId', EntityType::class, [
                'class' => Agence::class,
                'choice_label' => 'nom'
            ])
            ->add('marque', TextType::class)
            ->add('modele', TextType::class)
            ->add('immatriculation', TextType::class)
            ->add('volumeReservoir', IntegerType::class)
            ->add('couleur', TextType::class)
            ->add('dateAchat', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
            ->add('dateVente', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
            ->add('photo', UrlType::class,[
                'default_protocol' => 'file://'
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vehicule::class,
        ]);
    }
}