<?php


namespace App\Form;

use App\Entity\Vehicule;
use App\Repository\VehiculeRepository;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\Query\Expr;

class AddVehiculeFieldSubscriber implements EventSubscriberInterface
{


    public static function getSubscribedEvents(): array
    {
        return [FormEvents::PRE_SET_DATA => 'preSetData'];
    }

    public function preSetData(FormEvent $event): void
    {
        $louer = $event->getData();
        $form = $event->getForm();

        if ($louer->getDateDebutLocPrev() && $louer->getDateFinLocPrev()) {
            $form->add('vehiculeId', EntityType::class, [
                'class' => Vehicule::class,
                'query_builder' => function (VehiculeRepository $vehiculeRepository) use ($louer) {
                    return $vehiculeRepository->createQueryBuilder('v')
                        ->join('v.locations', 'l', Expr\Join::WITH, 'v.id = l.vehiculeId')
                        ->where('(:dateDebut NOT BETWEEN l.dateDebutLocPrev AND l.dateFinLocPrev) AND (:dateFin NOT BETWEEN l.dateDebutLocPrev AND l.dateFinLocPrev)')
                        ->orWhere('v.id NOT IN (SELECT (lo.vehiculeId) FROM App\Entity\Louer lo)')
                        ->setParameter('dateDebut', $louer->getDateDebutLocPrev())
                        ->setParameter('dateFin', $louer->getDateFinLocPrev());
                },
                'choice_attr' => [
                    'class' => 'm-1'
                ],
                'expanded' => true,

            ]);
        }
    }
}