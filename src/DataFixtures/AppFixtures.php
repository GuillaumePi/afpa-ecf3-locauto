<?php

namespace App\DataFixtures;

use App\Entity\Agence;
use App\Entity\Carburant;
use App\Entity\Categorie;
use App\Entity\Tarif;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Vehicule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $agence = new Agence();

        $agence->setNom('Nancy Centre')
            ->setAdresse('11 rue de Serre')
            ->setCodePostal('54000')
            ->setVille('Nancy')
            ->setTelephone($faker->phoneNumber)
            ->setEmail('contact@locauto.fr')
            ->setCreatedAt($faker->dateTimeBetween('-2 years', '- 6 months'))
            ->setCreatedBy('Admin');

        $manager->persist($agence);

        $carburant = new Carburant();

        $carburant->setLibelle('Essence')
            ->setPrix(1.60)
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($carburant);

        $type = new Type();

        $type->setLibelle('Voiture')
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($type);

        $categorieA = new Categorie();

        $categorieA->setTypeId($type)
            ->setCode('A')
            ->setLibelle('Voiture Eco')
            ->setDureeMiniPermis(3)
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($categorieA);

        $categorieB = new Categorie();

        $categorieB->setTypeId($type)
            ->setCode('B')
            ->setLibelle('Voiture Comfort')
            ->setDureeMiniPermis(3)
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($categorieB);

        $categorieC = new Categorie();

        $categorieC->setTypeId($type)
            ->setCode('C')
            ->setLibelle('Voiture Prestige')
            ->setDureeMiniPermis(5)
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($categorieC);

        $tarifA = new Tarif();

        $tarifA->setCategorieId($categorieA)
            ->setCaution(800)
            ->setAssurance(8)
            ->setJournee(15)
            ->setKilometre(0.10)
            ->setRemise(10)
            ->setWeekend(40)
            ->setDateDebut($agence->getCreatedAt())
            ->setDateFin(null)
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($tarifA);

        $tarifB = new Tarif();

        $tarifB->setCategorieId($categorieB)
            ->setCaution(900)
            ->setAssurance(10)
            ->setJournee(20)
            ->setKilometre(0.15)
            ->setRemise(10)
            ->setWeekend(55)
            ->setDateDebut($agence->getCreatedAt())
            ->setDateFin(null)
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($tarifB);

        $tarifC = new Tarif();

        $tarifC->setCategorieId($categorieC)
            ->setCaution(1000)
            ->setAssurance(12)
            ->setJournee(30)
            ->setKilometre(0.25)
            ->setRemise(10)
            ->setWeekend(85)
            ->setDateDebut($agence->getCreatedAt())
            ->setDateFin(null)
            ->setCreatedAt($agence->getCreatedAt())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($tarifC);

        $vehicule1 = new Vehicule();

        $vehicule1->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieA)
            ->setMarque('Peugeot')
            ->setModele('208 Like')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(45)
            ->setPhoto('./assets/images/208.png')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule1->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule1);

        $vehicule2 = new Vehicule();

        $vehicule2->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieA)
            ->setMarque('Renault')
            ->setModele('Twingo')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(45)
            ->setPhoto('./assets/images/twingo.png')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule2->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule2);

        $vehicule3 = new Vehicule();

        $vehicule3->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieA)
            ->setMarque('Citroen')
            ->setModele('C1')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(45)
            ->setPhoto('./assets/images/c1.png')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule3->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule3);

        $vehicule4 = new Vehicule();

        $vehicule4->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieB)
            ->setMarque('Peugeot')
            ->setModele('3008')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(55)
            ->setPhoto('./assets/images/3008.png')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule4->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule4);

        $vehicule5 = new Vehicule();

        $vehicule5->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieB)
            ->setMarque('Nissan')
            ->setModele('Qashqai')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(55)
            ->setPhoto('./assets/images/qashqai.png')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule5->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule5);

        $vehicule6 = new Vehicule();

        $vehicule6->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieB)
            ->setMarque('Jeep')
            ->setModele('Renegade')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(55)
            ->setPhoto('./assets/images/renegade.png')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule6->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule6);

        $vehicule7 = new Vehicule();

        $vehicule7->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieC)
            ->setMarque('Audi')
            ->setModele('A8')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(65)
            ->setPhoto('./assets/images/a8.jpg')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule7->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule7);

        $vehicule8 = new Vehicule();

        $vehicule8->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieC)
            ->setMarque('Audi')
            ->setModele('Q7')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(85)
            ->setPhoto('./assets/images/q7.png')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule8->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule8);

        $vehicule9 = new Vehicule();

        $vehicule9->setAgenceId($agence)
            ->setCarburantId($carburant)
            ->setCategorieId($categorieC)
            ->setMarque('Porsche')
            ->setModele('911')
            ->setImmatriculation($faker->bothify('??###??'))
            ->setCouleur('blanc')
            ->setVolumeReservoir(65)
            ->setPhoto('./assets/images/911.jpg')
            ->setRemarque(null)
            ->setDateAchat($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
            ->setDateVente(null)
            ->setCreatedAt($vehicule9->getDateAchat())
            ->setCreatedBy($agence->getCreatedBy());

        $manager->persist($vehicule9);

        for ($i = 0; $i < 20; $i++){
            $user = new User();
            $password = $this->encoder->encodePassword($user, '123456');

            $user->setCivilite($faker->title)
                ->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setEmail($faker->freeEmail)
                ->setPassword($password)
                ->setAdresse($faker->streetAddress)
                ->setCodePostal($faker->postcode)
                ->setVille($faker->city)
                ->setTelephone($faker->phoneNumber)
                ->setNumeroPermis($faker->numerify('############'))
                ->setDateObtentionPermis($faker->dateTimeBetween('-40 years', '-3 years'))
                ->setDateFinPermis(null)
                ->setIsVerified(true)
                ->setRole(1)
                ->setCreatedAt($faker->dateTimeBetween($agence->getCreatedAt(), '-15 days'))
                ->setCreatedBy('User');

            $manager->persist($user);
        }

        $manager->flush();
    }
}
