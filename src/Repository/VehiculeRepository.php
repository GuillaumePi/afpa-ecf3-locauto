<?php

namespace App\Repository;

use App\Entity\Vehicule;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Vehicule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vehicule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vehicule[]    findAll()
 * @method Vehicule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehiculeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vehicule::class);
    }

    /**
    * @return Vehicule[] Returns an array of Vehicule objects
    */
    public function findVehiculeDispo(DateTime $dateDebut, DateTime $dateFin)
    {
        return $this->createQueryBuilder('v', 'v.id')
            ->innerJoin('v.locations', 'l' )
            ->where('l.dateFinLocPrev < :dateDebut')
            ->andWhere('l.dateDebutLocPrev > :dateFin')
            ->setParameters(['dateDebut' => $dateDebut,
                'dateFin' => $dateFin
            ])
            ->getQuery()
            ->getResult();

    }
    // /**
    //  * @return Vehicule[] Returns an array of Vehicule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vehicule
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
