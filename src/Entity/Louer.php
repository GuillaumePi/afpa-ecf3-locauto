<?php

namespace App\Entity;

use App\Repository\LouerRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LouerRepository::class)
 */
class Louer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Vehicule::class, inversedBy="locations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vehiculeId;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="locations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDebutLocPrev;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateFinLocPrev;

    /**
     * @ORM\Column(type="smallint")
     */
    private $typeLoc;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDebutLocReel;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFinLocReel;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kilometrageInit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kilometrageFinal;

    /**
     * @ORM\Column(type="boolean")
     */
    private $assurance;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $volumeCarburantFinal;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantRetenuCaution;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $raisonRetenuCaution;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarque;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $createdBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVehiculeId(): ?Vehicule
    {
        return $this->vehiculeId;
    }

    public function setVehiculeId(?Vehicule $vehiculeId): self
    {
        $this->vehiculeId = $vehiculeId;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?UserInterface $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getDateDebutLocPrev(): ?\DateTimeInterface
    {
        return $this->dateDebutLocPrev;
    }

    public function setDateDebutLocPrev(\DateTimeInterface $dateDebutLocPrev): self
    {
        $this->dateDebutLocPrev = $dateDebutLocPrev;

        return $this;
    }

    public function getDateFinLocPrev(): ?\DateTimeInterface
    {
        return $this->dateFinLocPrev;
    }

    public function setDateFinLocPrev(\DateTimeInterface $dateFinLocPrev): self
    {
        $this->dateFinLocPrev = $dateFinLocPrev;

        return $this;
    }

    public function getTypeLoc(): ?int
    {
        return $this->typeLoc;
    }

    public function setTypeLoc(int $typeLoc): self
    {
        $this->typeLoc = $typeLoc;

        return $this;
    }

    public function getDateDebutLocReel(): ?\DateTimeInterface
    {
        return $this->dateDebutLocReel;
    }

    public function setDateDebutLocReel(?\DateTimeInterface $dateDebutLocReel): self
    {
        $this->dateDebutLocReel = $dateDebutLocReel;

        return $this;
    }

    public function getDateFinLocReel(): ?\DateTimeInterface
    {
        return $this->dateFinLocReel;
    }

    public function setDateFinLocReel(?\DateTimeInterface $dateFinLocReel): self
    {
        $this->dateFinLocReel = $dateFinLocReel;

        return $this;
    }

    public function getKilometrageInit(): ?int
    {
        return $this->kilometrageInit;
    }

    public function setKilometrageInit(?int $kilometrageInit): self
    {
        $this->kilometrageInit = $kilometrageInit;

        return $this;
    }

    public function getKilometrageFinal(): ?int
    {
        return $this->kilometrageFinal;
    }

    public function setKilometrageFinal(?int $kilometrageFinal): self
    {
        $this->kilometrageFinal = $kilometrageFinal;

        return $this;
    }

    public function getAssurance(): ?bool
    {
        return $this->assurance;
    }

    public function setAssurance(bool $assurance): self
    {
        $this->assurance = $assurance;

        return $this;
    }

    public function getVolumeCarburantFinal(): ?int
    {
        return $this->volumeCarburantFinal;
    }

    public function setVolumeCarburantFinal(?int $volumeCarburantFinal): self
    {
        $this->volumeCarburantFinal = $volumeCarburantFinal;

        return $this;
    }

    public function getMontantRetenuCaution(): ?float
    {
        return $this->montantRetenuCaution;
    }

    public function setMontantRetenuCaution(?float $montantRetenuCaution): self
    {
        $this->montantRetenuCaution = $montantRetenuCaution;

        return $this;
    }

    public function getRaisonRetenuCaution(): ?string
    {
        return $this->raisonRetenuCaution;
    }

    public function setRaisonRetenuCaution(?string $raisonRetenuCaution): self
    {
        $this->raisonRetenuCaution = $raisonRetenuCaution;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function __toString()
    {
        return $this->getCreatedAt()->format('d-m-Y');
    }


}
