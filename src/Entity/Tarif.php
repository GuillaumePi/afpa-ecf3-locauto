<?php

namespace App\Entity;

use App\Repository\TarifRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifRepository::class)
 */
class Tarif
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="tarifs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorieId;

    /**
     * @ORM\Column(type="float")
     */
    private $caution;

    /**
     * @ORM\Column(type="float")
     */
    private $assurance;

    /**
     * @ORM\Column(type="float")
     */
    private $journee;

    /**
     * @ORM\Column(type="float")
     */
    private $kilometre;

    /**
     * @ORM\Column(type="float")
     */
    private $weekend;

    /**
     * @ORM\Column(type="float")
     */
    private $remise;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFin;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $createdBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategorieId(): ?Categorie
    {
        return $this->categorieId;
    }

    public function setCategorieId(?Categorie $categorieId): self
    {
        $this->categorieId = $categorieId;

        return $this;
    }

    public function getCaution(): ?float
    {
        return $this->caution;
    }

    public function setCaution(float $caution): self
    {
        $this->caution = $caution;

        return $this;
    }

    public function getAssurance(): ?float
    {
        return $this->assurance;
    }

    public function setAssurance(float $assurance): self
    {
        $this->assurance = $assurance;

        return $this;
    }

    public function getJournee(): ?float
    {
        return $this->journee;
    }

    public function setJournee(float $journee): self
    {
        $this->journee = $journee;

        return $this;
    }

    public function getKilometre(): ?float
    {
        return $this->kilometre;
    }

    public function setKilometre(float $kilometre): self
    {
        $this->kilometre = $kilometre;

        return $this;
    }

    public function getWeekend(): ?float
    {
        return $this->weekend;
    }

    public function setWeekend(float $weekend): self
    {
        $this->weekend = $weekend;

        return $this;
    }

    public function getRemise(): ?float
    {
        return $this->remise;
    }

    public function setRemise(float $remise): self
    {
        $this->remise = $remise;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
