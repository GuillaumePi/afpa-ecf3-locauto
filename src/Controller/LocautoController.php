<?php

namespace App\Controller;

use App\Entity\Louer;
use App\Entity\User;
use App\Form\LocJourneeType;
use App\Form\LocWEType;
use App\Repository\AgenceRepository;
use App\Repository\LouerRepository;
use App\Repository\TarifRepository;
use App\Repository\UserRepository;
use App\Repository\VehiculeRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Snappy\Pdf;
use function Composer\Autoload\includeFile;

class LocautoController extends AbstractController
{
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * @Route("/", name="home")
     * @param Pdf $pdf
     * @param Request $request
     * @param VehiculeRepository $vehiculeRepository
     * @param MailerInterface $mailer
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     * @throws TransportExceptionInterface
     */
    public function index(Pdf $pdf,Request $request, VehiculeRepository $vehiculeRepository, MailerInterface $mailer, EntityManagerInterface $manager): Response
    {
        $vehicules = $vehiculeRepository->findAll();

        $user = $this->getUser();

        $this->mailer = $mailer;

        $louer = new Louer();

        $formJ = $this->createForm(LocJourneeType::class, $louer);
        $formJ->handleRequest($request);

        if ($formJ->isSubmitted() && $formJ->isValid()) {
            $louer->setUserId($user)
                ->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());

            $manager->persist($louer);
            $manager->flush();

            $this->addFlash('success', 'Votre réservation est bien prise en compte.');

            $filename ='reservation' . $louer->getId() . '-' . $louer->getUserId() . '-' . $louer->getVehiculeId() . '-' . $louer->getCreatedAt()->format('d-m-Y') . '.pdf' ;

            $pdf->generateFromHtml(
                $this->renderView(
                    'locauto/pdf_reservation.html.twig',
                    [
                        'louer' => $louer
                    ]
                ),
                'C:\dev\sfproject\\' . $filename
            );

            $email = (new TemplatedEmail())
                ->from(new Address('contact@locauto.fr', 'Reservation Email Bot'))
                ->to($louer->getUserId()->getEmail())
                ->subject('Votre reservation LocAuto')
                ->htmlTemplate('locauto/mail_reservation.html.twig')
                ->context([
                    'louer' => $louer,
                ])
                ->attachFromPath('C:\dev\sfproject\\' . $filename);
            $this->mailer->send($email);

            return $this->redirectToRoute('compte');
        }

        $formWE = $this->createForm(LocWEType::class, $louer);
        $formWE->handleRequest($request);

        if ($formWE->isSubmitted() && $formWE->isValid()) {
            $louer->setUserId($user)
                ->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());

            $manager->persist($louer);
            $manager->flush();

            $this->addFlash('success', 'Votre réservation est bien prise en compte.');

            $filename ='reservation' . $louer->getId() . '-' . $louer->getUserId() . '-' . $louer->getVehiculeId() . '-' . $louer->getCreatedAt()->format('d-m-Y') . '.pdf' ;

            $pdf->generateFromHtml(
                $this->renderView(
                    'locauto/pdf_reservation.html.twig',
                    [
                        'louer' => $louer
                    ]
                ),
                'C:\dev\sfproject\\' . $filename
            );

            $email = (new TemplatedEmail())
                ->from(new Address('contact@locauto.fr', 'Reservation Email Bot'))
                ->to($louer->getUserId()->getEmail())
                ->subject('Votre reservation LocAuto')
                ->htmlTemplate('locauto/mail_reservation.html.twig')
                ->context([
                    'louer' => $louer,
                ])
                ->attachFromPath('C:\dev\sfproject\\' . $filename);
            $this->mailer->send($email);

            return $this->redirectToRoute('compte');
        }

        //Météo

        if ($this->getUser()){
            $adresse = $this->getUser()->getCodePostal() . "%2C+" . $this->getUser()->getVille() . "%2C+France";
        }else{
            $adresse = "54000%2C+Nancy%2C+France";
        }

        $cle = "d394b44deca835a946b86ef89eed7271";
        $site = "https://maps.open-street.com/api/";
        $service = "geocoding/";
        $url = $site . $service . "?address=" . $adresse . "&sensor=false&key=" . $cle;

        $json = file_get_contents($url);

        $location_lat = json_decode($json)->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $location_lng = json_decode($json)->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};

        $today = 0;
        $demain = 1;
        $apdemain = 2;

        $cle_meteo = "6805afb9e484141a2ae84fe8176e3c6ba8a271748fd66c929eb812f27455f92a";
        $site_meteo = "https://api.meteo-concept.com/api/";
        $service_meteo = "forecast/daily/";

        $url_meteo_today = $site_meteo . $service_meteo . $today . "?token=" . $cle_meteo . "&latlng=" . $location_lat . "," . $location_lng . "&world=true";
        $url_meteo_demain = $site_meteo . $service_meteo . $demain . "?token=" . $cle_meteo . "&latlng=" . $location_lat . "," . $location_lng . "&world=true";
        $url_meteo_apdemain = $site_meteo . $service_meteo . $apdemain . "?token=" . $cle_meteo . "&latlng=" . $location_lat . "," . $location_lng . "&world=true";

        $meteo_today = json_decode(file_get_contents($url_meteo_today))->forecast;
        $meteo_demain = json_decode(file_get_contents($url_meteo_demain))->forecast;
        $meteo_apdemain = json_decode(file_get_contents($url_meteo_apdemain))->forecast;
        $meteo_ville = json_decode(file_get_contents($url_meteo_today))->city;

        $table_weather = [
            0=>"Soleil",
            1=>"Peu nuageux",
            2=>"Ciel voilé",
            3=>"Nuageux",
            4=>"Très nuageux",
            5=>"Couvert",
            6=>"Brouillard",
            7=>"Brouillard givrant",
            10=>"Pluie faible",
            11=>"Pluie modérée",
            12=>"Pluie forte",
            13=>"Pluie faible verglaçante",
            14=>"Pluie modérée verglaçante",
            15=>"Pluie forte verglaçante",
            16=>"Bruine",
            20=>"Neige faible",
            21=>"Neige modérée",
            22=>"Neige forte",
            30=>"Pluie et neige mêlées faibles",
            31=>"Pluie et neige mêlées modérées",
            32=>"Pluie et neige mêlées fortes",
            40=>"Averses de pluie locales et faibles",
            41=>"Averses de pluie locales",
            42=>"Averses locales et fortes",
            43=>"Averses de pluie faibles",
            44=>"Averses de pluie",
            45=>"Averses de pluie fortes",
            46=>"Averses de pluie faibles et fréquentes",
            47=>"Averses de pluie fréquentes",
            48=>"Averses de pluie fortes et fréquentes",
            60=>"Averses de neige localisées et faibles",
            61=>"Averses de neige localisées",
            62=>"Averses de neige localisées et fortes",
            63=>"Averses de neige faibles",
            64=>"Averses de neige",
            65=>"Averses de neige fortes",
            66=>"Averses de neige faibles et fréquentes",
            67=>"Averses de neige fréquentes",
            68=>"Averses de neige fortes et fréquentes",
            70=>"Averses de pluie et neige mêlées localisées et faibles",
            71=>"Averses de pluie et neige mêlées localisées",
            72=>"Averses de pluie et neige mêlées localisées et fortes",
            73=>"Averses de pluie et neige mêlées faibles",
            74=>"Averses de pluie et neige mêlées",
            75=>"Averses de pluie et neige mêlées fortes",
            76=>"Averses de pluie et neige mêlées faibles et nombreuses",
            77=>"Averses de pluie et neige mêlées fréquentes",
            78=>"Averses de pluie et neige mêlées fortes et fréquentes",
            100=>"Orages faibles et locaux",
            101=>"Orages locaux",
            102=>"Orages fort et locaux",
            103=>"Orages faibles",
            104=>"Orages",
            105=>"Orages forts",
            106=>"Orages faibles et fréquents",
            107=>"Orages fréquents",
            108=>"Orages forts et fréquents",
            120=>"Orages faibles et locaux de neige ou grésil",
            121=>"Orages locaux de neige ou grésil",
            122=>"Orages locaux de neige ou grésil",
            123=>"Orages faibles de neige ou grésil",
            124=>"Orages de neige ou grésil",
            125=>"Orages de neige ou grésil",
            126=>"Orages faibles et fréquents de neige ou grésil",
            127=>"Orages fréquents de neige ou grésil",
            128=>"Orages fréquents de neige ou grésil",
            130=>"Orages faibles et locaux de pluie et neige mêlées ou grésil",
            131=>"Orages locaux de pluie et neige mêlées ou grésil",
            132=>"Orages fort et locaux de pluie et neige mêlées ou grésil",
            133=>"Orages faibles de pluie et neige mêlées ou grésil",
            134=>"Orages de pluie et neige mêlées ou grésil",
            135=>"Orages forts de pluie et neige mêlées ou grésil",
            136=>"Orages faibles et fréquents de pluie et neige mêlées ou grésil",
            137=>"Orages fréquents de pluie et neige mêlées ou grésil",
            138=>"Orages forts et fréquents de pluie et neige mêlées ou grésil",
            140=>"Pluies orageuses",
            141=>"Pluie et neige mêlées à caractère orageux",
            142=>"Neige à caractère orageux",
            210=>"Pluie faible intermittente",
            211=>"Pluie modérée intermittente",
            212=>"Pluie forte intermittente",
            220=>"Neige faible intermittente",
            221=>"Neige modérée intermittente",
            222=>"Neige forte intermittente",
            230=>"Pluie et neige mêlées",
            231=>"Pluie et neige mêlées",
            232=>"Pluie et neige mêlées",
            235=>"Averses de grêle",
        ];

        $table_weather_logo = [
            0=>".\assets\images\animated\day.svg",
            1=>".\assets\images\animated\cloudy-day-1.svg",
            2=>".\assets\images\animated\cloudy-day-2.svg",
            3=>".\assets\images\animated\cloudy-day-2.svg",
            4=>".\assets\images\animated\cloudy-day-3.svg",
            5=>".\assets\images\animated\cloudy.svg",
            6=>".\assets\images\animated\cloudy.svg",
            7=>".\assets\images\animated\cloudy.svg",
            10=>".\assets\images\animated\/rainy-4.svg",
            11=>".\assets\images\animated\/rainy-5.svg",
            12=>".\assets\images\animated\/rainy-6.svg",
            13=>".\assets\images\animated\/rainy-4.svg",
            14=>".\assets\images\animated\/rainy-5.svg",
            15=>".\assets\images\animated\/rainy-6.svg",
            16=>".\assets\images\animated\/rainy-5.svg",
            20=>".\assets\images\animated\snowy-4.svg",
            21=>".\assets\images\animated\snowy-5.svg",
            22=>".\assets\images\animated\snowy-6.svg",
            30=>".\assets\images\animated\snowy-4.svg",
            31=>".\assets\images\animated\snowy-5.svg",
            32=>".\assets\images\animated\snowy-6.svg",
            40=>".\assets\images\animated\/rainy-1.svg",
            41=>".\assets\images\animated\/rainy-1.svg",
            42=>".\assets\images\animated\/rainy-1.svg",
            43=>".\assets\images\animated\/rainy-2.svg",
            44=>".\assets\images\animated\/rainy-2.svg",
            45=>".\assets\images\animated\/rainy-3.svg",
            46=>".\assets\images\animated\/rainy-2.svg",
            47=>".\assets\images\animated\/rainy-2.svg",
            48=>".\assets\images\animated\/rainy-3.svg",
            60=>".\assets\images\animated\snowy-1.svg",
            61=>".\assets\images\animated\snowy-1.svg",
            62=>".\assets\images\animated\snowy-1.svg",
            63=>".\assets\images\animated\snowy-1.svg",
            64=>".\assets\images\animated\snowy-1.svg",
            65=>".\assets\images\animated\snowy-1.svg",
            66=>".\assets\images\animated\snowy-1.svg",
            67=>".\assets\images\animated\snowy-1.svg",
            68=>".\assets\images\animated\snowy-1.svg",
            70=>".\assets\images\animated\snowy-1.svg",
            71=>".\assets\images\animated\snowy-1.svg",
            72=>".\assets\images\animated\snowy-1.svg",
            73=>".\assets\images\animated\snowy-1.svg",
            74=>".\assets\images\animated\snowy-1.svg",
            75=>".\assets\images\animated\snowy-1.svg",
            76=>".\assets\images\animated\snowy-1.svg",
            77=>".\assets\images\animated\snowy-1.svg",
            78=>".\assets\images\animated\snowy-1.svg",
            100=>".\assets\images\animated\/thunder.svg",
            101=>".\assets\images\animated\/thunder.svg",
            102=>".\assets\images\animated\/thunder.svg",
            103=>".\assets\images\animated\/thunder.svg",
            104=>".\assets\images\animated\/thunder.svg",
            105=>".\assets\images\animated\/thunder.svg",
            106=>".\assets\images\animated\/thunder.svg",
            107=>".\assets\images\animated\/thunder.svg",
            108=>".\assets\images\animated\/thunder.svg",
            120=>".\assets\images\animated\/thunder.svg",
            121=>".\assets\images\animated\/thunder.svg",
            122=>".\assets\images\animated\/thunder.svg",
            123=>".\assets\images\animated\/thunder.svg",
            124=>".\assets\images\animated\/thunder.svg",
            125=>".\assets\images\animated\/thunder.svg",
            126=>".\assets\images\animated\/thunder.svg",
            127=>".\assets\images\animated\/thunder.svg",
            128=>".\assets\images\animated\/thunder.svg",
            130=>".\assets\images\animated\/thunder.svg",
            131=>".\assets\images\animated\/thunder.svg",
            132=>".\assets\images\animated\/thunder.svg",
            133=>".\assets\images\animated\/thunder.svg",
            134=>".\assets\images\animated\/thunder.svg",
            135=>".\assets\images\animated\/thunder.svg",
            136=>".\assets\images\animated\/thunder.svg",
            137=>".\assets\images\animated\/thunder.svg",
            138=>".\assets\images\animated\/thunder.svg",
            140=>".\assets\images\animated\/rainy-1.svg",
            141=>".\assets\images\animated\/rainy-1.svg",
            142=>".\assets\images\animated\/rainy-1.svg",
            210=>".\assets\images\animated\/rainy-1.svg",
            211=>".\assets\images\animated\/rainy-1.svg",
            212=>".\assets\images\animated\/rainy-1.svg",
            220=>".\assets\images\animated\/rainy-1.svg",
            221=>".\assets\images\animated\/rainy-1.svg",
            222=>".\assets\images\animated\/rainy-1.svg",
            230=>".\assets\images\animated\/rainy-1.svg",
            231=>".\assets\images\animated\/rainy-1.svg",
            232=>".\assets\images\animated\/rainy-1.svg",
            235=>".\assets\images\animated\/rainy-7.svg",
        ];

        return $this->render('locauto/index.html.twig', [
            'locJourneeForm' => $formJ->createView(),
            'locWEForm' => $formWE->createView(),
            'vehicules' => $vehicules,
            'meteo_today' => $meteo_today,
            'meteo_demain' => $meteo_demain,
            'meteo_apdemain' => $meteo_apdemain,
            'table_meteo' => $table_weather,
            'table_meteo_logo' => $table_weather_logo,
            'station' => $meteo_ville
        ]);
    }

    /**
     * @Route("/agence", name="agence")
     */
    public function agence(): Response
    {
        return $this->render('locauto/agence.html.twig');
    }

    /**
     * @Route("/vehicules", name="vehicules")
     */
    public function vehicules(): Response
    {
        return $this->render('locauto/vehicules.html.twig');
    }

    /**
     * @Route("/services", name="services")
     */
    public function services(): Response
    {
        return $this->render('locauto/services.html.twig');
    }

    /**
     * @Route("/infos", name="infos")
     */
    public function infos(): Response
    {
        return $this->render('locauto/infos.html.twig');
    }

    /**
     * @Route("/compte", name="compte")
     */
    public function compte(): Response
    {
        return $this->render('locauto/compte.html.twig');
    }

    /**
     * @Route("/compte/historique", name="historique")
     */
    public function historique(): Response
    {
        return $this->render('locauto/historique.html.twig');
    }
}

