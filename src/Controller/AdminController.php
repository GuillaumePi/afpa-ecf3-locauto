<?php

namespace App\Controller;

use App\Entity\Carburant;
use App\Entity\Categorie;
use App\Entity\Louer;
use App\Entity\Tarif;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Vehicule;
use App\Form\CarburantFormType;
use App\Form\CategorieFormType;
use App\Form\LocationFormType;
use App\Form\TarifFormType;
use App\Form\TypeFormType;
use App\Form\UserFormType;
use App\Form\VehiculeFormType;
use App\Repository\CarburantRepository;
use App\Repository\CategorieRepository;
use App\Repository\LouerRepository;
use App\Repository\TarifRepository;
use App\Repository\TypeRepository;
use App\Repository\UserRepository;
use App\Repository\VehiculeRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Snappy\Pdf;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{

    /**
     * @Route("/admin", name="admin")
     * @param UserRepository $userRepository
     * @param LouerRepository $louerRepository
     * @param VehiculeRepository $vehiculeRepository
     * @param CategorieRepository $categorieRepository
     * @param TypeRepository $typeRepository
     * @param TarifRepository $tarifRepository
     * @param CarburantRepository $carburantRepository
     * @return Response
     */
    public function index(UserRepository $userRepository, LouerRepository $louerRepository, VehiculeRepository $vehiculeRepository, CategorieRepository $categorieRepository, TypeRepository $typeRepository, TarifRepository $tarifRepository, CarburantRepository $carburantRepository): Response
    {
        $users = $userRepository->findAll();
        $locations = $louerRepository->findAll();
        $vehicules = $vehiculeRepository->findAll();
        $categories = $categorieRepository->findAll();
        $types = $typeRepository->findAll();
        $tarifs = $tarifRepository->findAll();
        $carburants = $carburantRepository->findAll();

        $this->denyAccessUnlessGranted('ROLE_ADMIN', 'Accès non authorisé', 'Un utilisateur a essayé d\'accéder à la page Admin sans avoir les droits.');

        return $this->render('admin/index.html.twig', [
            'users' => $users,
            'locations' => $locations,
            'vehicules' => $vehicules,
            'categories' => $categories,
            'types' => $types,
            'tarifs' => $tarifs,
            'carburants' => $carburants
        ]);
    }

    /**
     * @Route ("/admin/users", name="admin_users")
     * @param UserRepository $userRepository
     * @return Response
     */
    public function utilisateur(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();

        return $this->render('admin/utilisateur.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/users/new", name="admin_user_create")
     * @Route("/admin/users/{id}/edit", name="admin_user_edit", requirements={"id"="\d+"})
     * @param User|null $user
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function formUtilisateur(User $user = null, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$user) {
            $user = new User();
        }

        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$user->getId()) {
                $user->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                    ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());
            }
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'Effectué !');

            return $this->redirectToRoute('admin_user_edit', ['id' => $user->getId()]);
        }

        return $this->render('admin/create_user.html.twig', [
            'userForm' => $form->createView(),
            'editMode' => $user->getId() !== null,
            'user' => $user,
        ]);
    }

    /**
     * @Route ("/admin/users/{id}/historique", name="admin_users_histo", requirements={"id"="\d+"})
     * @param User $user
     * @param UserRepository $userRepository
     * @return Response
     */
    public function utilisateurHistorique(UserRepository $userRepository, User $user): Response
    {

        return $this->render('admin/utilisateur_histo.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route ("/admin/locations", name="admin_locations")
     * @param LouerRepository $locationRepository
     * @return Response
     */
    public function location(LouerRepository $locationRepository): Response
    {
        $locations = $locationRepository->findAll();

        return $this->render('admin/location.html.twig', [
            'locations' => $locations
        ]);
    }

    /**
     * @Route("/admin/locations/new", name="admin_location_create")
     * @Route("/admin/locations/{id}/edit", name="admin_location_edit", requirements={"id"="\d+"})
     * @param Louer|null $location
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function formLocation(Louer $location = null, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$location) {
            $location = new Louer();
        }

        $form = $this->createForm(LocationFormType::class, $location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$location->getId()) {
                $location->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                    ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());
            }
            $manager->persist($location);
            $manager->flush();

            $this->addFlash('success', 'Effectué !');

            return $this->redirectToRoute('admin_location_edit', ['id' => $location->getId()]);
        }

        return $this->render('admin/create_location.html.twig', [
            'locationForm' => $form->createView(),
            'editMode' => $location->getId() !== null,
            'location' => $location,
        ]);
    }

    /**
     * @Route ("/admin/vehicules", name="admin_vehicules")
     * @param VehiculeRepository $vehiculeRepository
     * @return Response
     */
    public function vehicule(VehiculeRepository $vehiculeRepository): Response
    {
        $vehicules = $vehiculeRepository->findAll();

        return $this->render('admin/vehicule.html.twig', [
            'vehicules' => $vehicules
        ]);
    }

    /**
     * @Route("/admin/vehicules/new", name="admin_vehicule_create")
     * @Route("/admin/vehicules/{id}/edit", name="admin_vehicule_edit", requirements={"id"="\d+"})
     * @param Vehicule|null $vehicule
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function formVehicule(Vehicule $vehicule = null, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$vehicule) {
            $vehicule = new Vehicule();
        }

        $form = $this->createForm(VehiculeFormType::class, $vehicule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$vehicule->getId()) {
                $vehicule->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                    ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());
            }
            $manager->persist($vehicule);
            $manager->flush();

            $this->addFlash('success', 'Effectué !');

            return $this->redirectToRoute('admin_vehicule_edit', ['id' => $vehicule->getId()]);
        }

        return $this->render('admin/create_vehicule.html.twig', [
            'vehiculeForm' => $form->createView(),
            'editMode' => $vehicule->getId() !== null,
            'vehicule' => $vehicule
        ]);
    }

    /**
     * @Route ("/admin/vehicules/{id}/historique", name="admin_vehicules_histo", requirements={"id"="\d+"})
     * @param VehiculeRepository $vehiculeRepository
     * @param Vehicule $vehicule
     * @return Response
     */
    public function vehiculeHistorique(VehiculeRepository $vehiculeRepository, Vehicule $vehicule): Response
    {

        return $this->render('admin/vehicule_histo.html.twig', [
            'vehicule' => $vehicule,
        ]);
    }

    /**
     * @Route ("/admin/categories", name="admin_categories")
     * @param CategorieRepository $categorieRepository
     * @return Response
     */
    public function categorie(CategorieRepository $categorieRepository): Response
    {
        $categories = $categorieRepository->findAll();

        return $this->render('admin/categorie.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/categories/new", name="admin_categorie_create")
     * @Route("/admin/categories/{id}/edit", name="admin_categorie_edit", requirements={"id"="\d+"})
     * @param Categorie|null $categorie
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function formCategorie(Categorie $categorie = null, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$categorie) {
            $categorie = new Categorie();
        }

        $form = $this->createForm(CategorieFormType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$categorie->getId()) {
                $categorie->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                    ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());
            }
            $manager->persist($categorie);
            $manager->flush();

            $this->addFlash('success', 'Effectué !');

            return $this->redirectToRoute('admin_vehicule_edit', ['id' => $categorie->getId()]);
        }

        return $this->render('admin/create_categorie.html.twig', [
            'categorieForm' => $form->createView(),
            'editMode' => $categorie->getId() !== null,
            'categorie' => $categorie
        ]);
    }

    /**
     * @Route ("/admin/types", name="admin_types")
     * @param TypeRepository $typeRepository
     * @return Response
     */
    public function type(TypeRepository $typeRepository): Response
    {
        $types = $typeRepository->findAll();

        return $this->render('admin/type.html.twig', [
            'types' => $types
        ]);
    }

    /**
     * @Route("/admin/types/new", name="admin_type_create")
     * @Route("/admin/types/{id}/edit", name="admin_type_edit", requirements={"id"="\d+"})
     * @param Type|null $type
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function formType(Type $type = null, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$type) {
            $type = new Type();
        }

        $form = $this->createForm(TypeFormType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$type->getId()) {
                $type->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                    ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());
            }
            $manager->persist($type);
            $manager->flush();

            $this->addFlash('success', 'Effectué !');

            return $this->redirectToRoute('admin_type_edit', ['id' => $type->getId()]);
        }

        return $this->render('admin/create_type.html.twig', [
            'typeForm' => $form->createView(),
            'editMode' => $type->getId() !== null,
            'type' => $type
        ]);
    }

    /**
     * @Route ("/admin/tarifs", name="admin_tarifs")
     * @param TarifRepository $tarifRepository
     * @return Response
     */
    public function tarif(TarifRepository $tarifRepository): Response
    {
        $tarifs = $tarifRepository->findAll();

        return $this->render('admin/tarif.html.twig', [
            'tarifs' => $tarifs
        ]);
    }

    /**
     * @Route("/admin/tarifs/new", name="admin_tarif_create")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function formTarif(Request $request, EntityManagerInterface $manager): Response
    {

        $tarif = new Tarif();


        $form = $this->createForm(TarifFormType::class, $tarif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $tarif->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());

            $manager->persist($tarif);
            $manager->flush();

            $this->addFlash('success', 'Effectué !');

            return $this->redirectToRoute('admin_tarifs');
        }

        return $this->render('admin/create_tarif.html.twig', [
            'tarifForm' => $form->createView(),
            'tarif' => $tarif
        ]);
    }

    /**
     * @Route ("/admin/carburants", name="admin_carburants")
     * @param CarburantRepository $carburantRepository
     * @return Response
     */
    public function carburant(CarburantRepository $carburantRepository): Response
    {
        $carburants = $carburantRepository->findAll();

        return $this->render('admin/carburant.html.twig', [
            'carburants' => $carburants
        ]);
    }

    /**
     * @Route("/admin/carburants/new", name="admin_carburant_create")
     * @Route("/admin/carburants/{id}/edit", name="admin_carburant_edit", requirements={"id"="\d+"})
     * @param Carburant|null $carburant
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function formCarburant(Carburant $carburant = null, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$carburant) {
            $carburant = new Carburant();
        }

        $form = $this->createForm(CarburantFormType::class, $carburant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$carburant->getId()) {
                $carburant->setCreatedAt(new DateTime('now', new DateTimeZone('Europe/Paris')))
                    ->setCreatedBy($this->getUser()->getNom() . ' ' . $this->getUser()->getPrenom());
            }
            $manager->persist($carburant);
            $manager->flush();

            $this->addFlash('success', 'Effectué !');

            return $this->redirectToRoute('admin_carburant_edit', ['id' => $carburant->getId()]);
        }

        return $this->render('admin/create_carburant.html.twig', [
            'carburantForm' => $form->createView(),
            'editMode' => $carburant->getId() !== null,
            'carburant' => $carburant
        ]);
    }

    /**
     * @Route("/admin/locations/{id}/facture", name="admin_location_facture", requirements={"id"="\d+"})
     * @param Pdf $pdf
     * @param MailerInterface $mailer
     * @param Louer $louer
     * @param TarifRepository $tarifRepository
     * @param $factLoc
     * @param $factKm
     * @throws TransportExceptionInterface
     */
    public function envoiFacture(Pdf $pdf, MailerInterface $mailer, Louer $louer, TarifRepository $tarifRepository, $factLoc = null, $factKm = null): Response
    {
        $mailer1 = $mailer;

        $tarifs = $tarifRepository->findOneBy(['categorieId' => $louer->getVehiculeId()->getCategorieId()]);
        $dateDepart = $louer->getDateDebutLocReel()->setTime(0, 0);
        $dateRetour = $louer->getDateFinLocReel()->setTime(0, 0);

        $dureeLoc = $dateDepart->diff($dateRetour)->format('%a');
        $distanceLoc = $louer->getKilometrageFinal() - $louer->getKilometrageInit();
        $typeLoc = $louer->getTypeLoc();
        $assur = $louer->getAssurance();
        $carburantRestant = $louer->getVolumeCarburantFinal();
        $volReservoir = $louer->getVehiculeId()->getVolumeReservoir();
        $caution = $louer->getMontantRetenuCaution();
        if ($typeLoc == 1) {
            $factLoc = $dureeLoc * $tarifs->getJournee();
            $factKm = $distanceLoc * $tarifs->getKilometre();
        } elseif ($typeLoc == 2) {
            $factLoc = $tarifs->getWeekend();
            if ($distanceLoc > 1000) {
                $factKm = ($distanceLoc - 1000) * $tarifs->getKilometre();
            } else {
                $factKm = 0;
            }
        }
        if ($assur == 1) {
            $factAssur = $dureeLoc * $tarifs->getAssurance();
        } else {
            $factAssur = 0;
        }
        if (!empty($carburantRestant) && $carburantRestant < $volReservoir) {
            $factCarbu = ($louer->getVehiculeId()->getVolumeReservoir() - $louer->getVolumeCarburantFinal()) * $louer->getVehiculeId()->getCarburantId()->getPrix();
        } else {
            $factCarbu = 0;
        }
        if (!empty($caution)) {
            $factCaution = $louer->getMontantRetenuCaution();
        } else {
            $factCaution = 0;
        }


        $filename = 'facture' . $louer->getId() . '-' . $louer->getUserId() . '-' . $louer->getVehiculeId() . '-' . $louer->getCreatedAt()->format('d-m-Y') . '.pdf';

        $pdf->generateFromHtml(
            $this->renderView(
                'locauto/pdf_facturation.html.twig',
                [
                    'louer' => $louer,
                    'dureeLoc' => $dureeLoc,
                    'distanceLoc' => $distanceLoc,
                    'factLoc' => $factLoc,
                    'factKm' => $factKm,
                    'factAssur' => $factAssur,
                    'factCarbu' => $factCarbu,
                    'factCaution' => $factCaution
                ]
            ),
            'C:\dev\sfproject\\' . $filename
        );

        $email = (new TemplatedEmail())
            ->from(new Address('contact@locauto.fr', 'Facturation Email Bot'))
            ->to($louer->getUserId()->getEmail())
            ->subject('Votre facture LocAuto')
            ->htmlTemplate('locauto/mail_facturation.html.twig')
            ->context([
                'louer' => $louer,
            ])
            ->attachFromPath('C:\dev\sfproject\\' . $filename);
        $mailer1->send($email);

        return $this->redirectToRoute('admin_location_facture', ['id' => $louer->getId()]);
    }
}
